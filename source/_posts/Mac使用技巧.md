layout: post
title: Mac使用技巧
comments: true
date: 2017-10-24 11:38:00  +0800
updated:
tags:
- Mac
- 软件技巧
category:
- 技术

---

![](https://gitlab.com/MelerPaine/images/raw/master/Web/sweet-ice-cream-photography-375595-small.jpg)

### 显示隐藏文件
在终端中执行以下命令：
```shell
defaults write com.apple.finder AppleShowAllFiles -bool true
```
将true改为false变为隐藏

### 重新启动 Finder
打开终端执行如下命令：`killall Finder`

<!-- more -->

### 打开任何来源的 App
在系统的设置项中，安全性部分，打开任何来源的 App 选项默认使隐藏的，使用以下命令打开：
```sh
sudo spctl --master-disable
```

### 复制目录
```sh
\cp path1\* path2\ -r -f # 将 path1 下的所有文件递归复制到 path2 目录下，且不提示。cp 前加 \ 表示使用原始命令而不是别名（系统默认使用的cp命令 为 cp -i 的别名）
```
### 常用快捷键
进入浏览器地址栏：command+L。

### 提高Dock弹出速度
使用如下命令可使Dock瞬间弹出： 
`defaults write com.apple.Dock autohide-delay -float 0 && killall Dock`
恢复原速度： 
`defaults delete com.apple.Dock autohide-delay && killall Dock`
[参考链接](http://www.ihunter.me/mac%E4%B8%AD%E5%87%8F%E5%B0%91dock%E8%87%AA%E5%8A%A8%E9%9A%90%E8%97%8F%E7%9A%84%E5%BB%B6%E8%BF%9F%E6%97%B6%E9%97%B4.html)

### Yosemite:恢复硬盘多分区功能以及开机按option 时出现恢复分区
升级到Yosemite时，开机默认会隐藏恢复分区，如下任一操作都可以进入恢复分区：
1.开机按住Command+R；
2.开机按住Option，出现启动磁盘选单后，按一下Command+R。

通过以下命令，可对磁盘进行转换，开机可直接看到恢复分区， [参考链接](http://bbs.feng.com/read-htm-tid-8148508.html) ：
`diskutil cs revert /`

### 修改hosts
文件路径：/private/etc/hosts
修改方法：拷贝至用户目录，修改后再覆盖粘贴。

### 重置Mac登录密码
一、命令行解决方法。
开机， 启动时按“command+s”这时进入单一用户模式（Single user model）。
Mac OS的单一用户模式有准入特权（Root access privilege）而不要求根密码（root password）。
出现像DOS一样的提示符 \#root>。
在#root>下逐步输入以下命令，注意空格，大小写！
```shell
/sbin/mount -uaw 回车
rm /var/db/.applesetupdone 回车（注意“.”前没有空格）
reboot 回车
```
命令的解释：第一步是加载文件系统（读/写）
第二步是删除初始化设置时的osx生成的隐藏文件
第三步是重启

重启开机后出现类似装机时的欢迎界面。别担心，东西没丢。
就像第一次安装一样， 重新建立一个新的Mac OS管理员账号。
然后在新的管理员下打开系统预制 － 账户，打开最下面的锁，询问密码时，用新的管理员密码登录。
会看到至少两个账号，新的管理员的帐号和原来的帐号。
点中原来的账号，选：密码 － 更改密码……（一切，从此改变）
你不必有原先的Mac OS密码就直接可以设新密码了。
点下面的登陆选项 (小房子)，选中 自动以右边的身份登陆， 同时在下拉菜单中选你原先的账号。
重启， 大功告成。至此破解Mac OS开机密码工作完成。
如果不喜欢多出一个账号， 删除它（将删除原用户文件夹下的资料）。
系统预制 － 账户，选新的管理员帐号， 点一下锁上面的减号。

二、官方解决方法。
找出电脑原配的系统盘，找不到就借一张或者刻录一张，重启电脑，启动的时候按C键，选好语言后进入安装的时候，点击“常用工具”，里面有一项是“重设密码”，这时就可以重新设定Mac OS系统的管理员密码了。

三、Recovery模式终端更改用户账户密码
其实这个方法较前两个是比较简单方便的，实质就是进入Recovery来使用终端修改密码。
第一种方法：开机按CMD+R进入网络恢复模式，等待地球转，进度条读完，进入实用工具-终端-输入resetpassword就可以直接修改密码了。
第二种方法：开机按住Option，再按按CMD+R，进入Recovery（恢复），实用工具-终端-输入resetpassword修改密码。

### 提取 Mac OS X / iOS 应用图标的技巧
打开 Finder，右键点击「显示简介」，点击应用图标，按command + c，打开预览工具，点击菜单栏「文件」中「从剪贴板新建」选项，将得到一系列分辨率大小的图标，导出即可使用。

### BasicIPv6ValidationError解决办法
打开终端按如下命令操作
1.列出你的网卡
`networksetup -listallnetworkservices`
2.关闭ipv6
`networksetup -setv6off "你网卡名字"`
3.设置ip地址
`networksetup -setmanual "网卡名字" 192.168.31.2 255.255.255.0 192.168.1.1`

### 文件 MD5 及 SHA1 校验
打开终端按如下命令操作
校验 md5：`md5 文件名`
校验 SHA1：`shasum 文件名`

> 本文将持续更新，敬请关注。

----
题图: [Palm trees - Sweet Ice Cream Photography@unsplash](https://unsplash.com/photos/h8HDSbCFPM0)

**“你的喜爱就是我的动力，欢迎各位打赏”**
![微信赞赏码](https://user-images.githubusercontent.com/6366798/31317194-4971732c-ac6f-11e7-85b1-4ab24d057d8b.JPG)

